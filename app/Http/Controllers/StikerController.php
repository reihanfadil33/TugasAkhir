<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stiker;

class StikerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$data['stiker'] = Stiker::all();
		$data['bahan'] = \App\Bahan::all();
		//$data['stikera'] = \DB::table('t_stiker')->join('t_pembayaran','t_pembayaran.id_stiker','=','t_stiker.id_stiker')->get();
		$data['jumlah'] = Stiker::count();
        return view('stiker.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$data['bahan'] = \App\Bahan::all();
        return view('stiker.form')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
          'harga_stiker' => 'required|max:15',
          'gambar_stiker' => 'required|mimes:jpeg,png,jpg'
        ];
        $this->validate($request,$rules);
        $input = $request->all();
        //validation foto upload
        if($request->hasFile('gambar_stiker') && $request->file('gambar_stiker')->isValid()){
          $filename = $request->file('gambar_stiker')->getClientOriginalName();
          $request->file('gambar_stiker')->storeAs('',$filename);
          $input['gambar_stiker'] = $filename;
        }
		$status = \App\Stiker::create($input);

        if($status) return redirect('stiker/')->with('success','Data Berhasil Ditambahkan');
        else return redirect('stiker/add')->with('error','Data Gagal ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$rules = [
			'gambar_stiker' =>'required|mimes:jpeg,png|max:1024',
			'harga_stiker' => 'required'
		];
        $data['edit'] = true;
		$data['bahan'] = \App\Bahan::all();
		$data['result'] = \App\Stiker::where('id_stiker',$id)->first();
		return view('stiker.form')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	 $rules = [
          'harga_stiker' => 'required|max:15',
          'gambar_stiker' => 'required|mimes:jpeg,png,jpg'
        ];
        $this->validate($request,$rules);
        $input = $request->all();
        //validation foto upload
        if($request->hasFile('gambar_stiker') && $request->file('gambar_stiker')->isValid()){
          $filename = $request->file('gambar_stiker')->getClientOriginalName();
          $request->file('gambar_stiker')->storeAs('',$filename);
          $input['gambar_stiker'] = $filename;
        }
		$status = \App\Stiker::where('id_stiker',$id)->first()->update($input);

        if($status) return redirect('stiker/')->with('success','Data Berhasil Ditambahkan');
        else return redirect('stiker/add')->with('error','Data Gagal ditambahkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = \App\Stiker::where('id_stiker',$id)->first()->delete();
		
		if ($status){
			return redirect('stiker')->with('success','data berhasil dihapus');
		} else {
			return redirect('stiker')->with('error','data gagal dihapus');
		}
    }
}
