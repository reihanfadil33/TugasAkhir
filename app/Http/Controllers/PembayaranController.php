<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pembayaran;

class PembayaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$data['pembayaran'] = Pembayaran::all();
        return view('pembayaran.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pembayaran.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all(); // Mengambil semua request dari form
		
		$status = \App\Pembayaran::create($input);
		
		if ($status){
			return redirect('pembayaran')->with('success','data berhasil ditambahkan');
		} else {
			return redirect('pembayaran/add')->with('error','data gagal ditambahkan');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['edit'] = true;
		$data['result'] = \App\pembayaran::where('id_pembayaran',$id)->first();
		return view('pembayaran.form')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$input = $request->all(); // Mengambil semua request dari form
		
		$status = \App\pembayaran::where('id_pembayaran',$id)->first()->update($input);
		
		if ($status){
			return redirect('pembayaran')->with('success','data berhasil diubah');
		} else {
			return redirect('pembayaran/'. $id .'/edit')->with('error','data gagal diubah');
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = \App\Pembayaran::where('id_pembayaran',$id)->first()->delete();
		
		if ($status){
			return redirect('pembayaran')->with('success','data berhasil dihapus');
		} else {
			return redirect('pembayaran')->with('error','data gagal dihapus');
		}
    }
}
