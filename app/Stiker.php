<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stiker extends Model
{
    protected $table = 't_stiker';
	public $primaryKey = 'id_stiker';
	protected $fillable = ['id_stiker','gambar_stiker','harga_stiker','id_bahan'];
	
	public $timestamps = false;
}
