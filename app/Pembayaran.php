<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $table = 't_pembayaran';
	public $primaryKey = 'id_pembayaran';
	protected $fillable = ['id_pembayaran','id_user','id_stiker','pcs','totalharga','status'];
	
	public $timestamps = false;
}
