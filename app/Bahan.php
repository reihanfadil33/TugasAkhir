<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bahan extends Model
{
    protected $table = 't_bahan';
	public $primaryKey = 'id_bahan';
	protected $fillable = ['kertas'];
	
	public $timestamps = false;
}
