<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::auth();
Route::group(['middleware'=>'auth'], function(){

  Route::get('/',function(){
    return view('login');
  });
  
Route::get('/home', 'StikerController@index');
Route::get('/', 'StikerController@index');

Route::get('/stiker', 'StikerController@index');
Route::get('/stiker/add', 'StikerController@create');
Route::post('/stiker/add', 'StikerController@store');
Route::get('/stiker/{id}/edit', 'StikerController@edit');
Route::patch('/stiker/{id}/edit', 'StikerController@update');
Route::delete('/stiker/{id}/delete', 'StikerController@destroy');

Route::get('/bahan', 'BahanController@index');
Route::get('/bahan/add', 'BahanController@create');
Route::post('/bahan/add', 'BahanController@store');
Route::get('/bahan/{id}/edit', 'BahanController@edit');
Route::patch('/bahan/{id}/edit', 'BahanController@update');
Route::delete('/bahan/{id}/delete', 'BahanController@destroy');

});