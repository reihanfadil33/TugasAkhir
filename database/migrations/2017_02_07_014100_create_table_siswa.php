<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSiswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_siswa', function(Blueprint $table){
			$table->increments('id_siswa');
			$table->INTEGER('nis')->unique();
			$table->string('nama', 36);
			$table->string('jk', 12);
			$table->string('alamat', 36);
			$table->string('notelp', 16);
			$table->string('agama', 36);
			$table->string('id_kelas', 12);
			
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_siswa');
    }
}
