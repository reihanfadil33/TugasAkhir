@extends ('template.header')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
		Cetak Printing
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Cetak Stiker</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	@if(session('success'))
	  <div class="alert alert-success">
		<p>{{ session('success') }}</p>
	  </div>
	 @endif
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <a href="{{ url('stiker/add')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data </a>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
	<?php $no=1;?>
	@foreach($stiker as $row)
	<div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                ID Stiker : {{ $row->id_stiker }} 
            </div>
            <div class="panel-body">
				<img src="{{ ('public') }}/uploads/{{ $row->gambar_stiker }}" class="img-thumbnail" alt="User Image">
			</div>
            <div class="panel-footer">
				<span class="pull-left">Harga : {{ $row->harga_stiker }}</span>
				
				<div class="clearfix"></div>	
			</div>
			<div class="panel-footer-edit">
			
				<span class="pull-left">Id Kertas : {{ $row->id_bahan }}</span>
				<form method="post" action="{{ url('stiker/' . $row->id_stiker . '/delete')}}">
					<span class="pull-right">
						<a href="{{ url('stiker/' . $row->id_stiker . '/edit')}}" class="btn btn-primary"><i class="fa  fa-pencil"></i></a>
						
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<button class="btn btn-danger"><i class="fa  fa-trash"></i></button>
					</span>
				</form>
				<div class="clearfix"></div>	
			</div>					
        </div>
    </div>
	@endforeach
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
         
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection