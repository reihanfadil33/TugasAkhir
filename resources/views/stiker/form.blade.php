@extends('template.header')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
	  {{ empty($edit) ? 'Tambah' : 'Edit '}} Stiker
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('stiker') }}">Stiker</a></li>
        <li class="active">{{ empty($edit) ? 'Tambah' : 'Edit '}} Stiker</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	@if(session('error'))
	  <div class="alert alert-danger">
		<p>{{ session('error') }}</p>
	  </div>
	 @endif
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
			<a href="{{ url('stiker') }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Kembali</a>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="container">
			<div class="row">
				<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{ empty($edit) ? url('stiker/add') : url('stiker/' . $result->id_stiker . '/edit')}}" >
					{{ csrf_field() }}
					
					@if(!empty($edit))
					{{ method_field('PATCH') }}
					@endif
		@if(!empty($edit))
		<div class="col-sm-4">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Curent Image
				</div>
				<div class="panel-body">
					<img src="{{ asset('public/uploads/'.$result->gambar_stiker)}}" class="img-thumbnail" alt="User Image">
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label">Gambar Stiker</label>
			<div class="col-sm-5">
				<input type="file" name="gambar_stiker" class="form-control" placeholder="Gambar Stiker" value="{{ @$result->gambar_stiker }} " required/><br>
			</div>
			
			<label class="col-sm-2 control-label">Harga Stiker</label>
			<div class="col-sm-5">
				<input type="text" name="harga_stiker" class="form-control" placeholder="Harga Stiker"  value="{{ @$result->harga_stiker }} " required /><br>
			</div>
			<label class="col-sm-2 control-label">Bahan :</label>
			<div class="col-sm-5">
				<select class="form-control" name="id_bahan">
            @foreach($bahan as $row)
            <option value="{{$row->id_bahan}}" {{@$row->id_bahan == @$result->id_bahan ? 'selected' : ''}}>{{$row->kertas}}</option>
            @endforeach
          </select>
			</div>			
			<label class="col-sm-2 control-label"></label>
			<div class="col-sm-5">
				<button type="submit" class="btn btn-primary" value="Submit" name="submit">Submit</button>
			</div>
		</div>
		@endif
		@if(empty($edit))
		<div class="form-group">
			<label class="col-sm-2 control-label">Gambar Stiker</label>
			<div class="col-sm-4">
				<input type="file" name="gambar_stiker" class="form-control"/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Harga Stiker</label>
			<div class="col-sm-4">
				<input type="text" name="harga_stiker" class="form-control" required />
			</div>
		</div>	
		<div class="form-group">
			<label class="col-sm-2 control-label">Bahan :</label>
			<div class="col-sm-4">
				<select class="form-control" name="id_bahan">
            @foreach($bahan as $row)
            <option value="{{$row->id_bahan}}" {{@$row->id_bahan == @$result->id_bahan ? 'selected' : ''}}>{{$row->kertas}}</option>
            @endforeach
          </select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"></label>
			<div class="col-sm-4">
				<button type="submit" class="btn btn-primary" value="Submit" name="submit">Submit</button>
			</div>
		</div>
		@endif
	</form>
			</div>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection	