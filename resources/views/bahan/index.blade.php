@extends ('template.header')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Info Jenis Kertas
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Jenis Kertas</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	@if(session('success'))
	  <div class="alert alert-success">
		<p>{{ session('success') }}</p>
	  </div>
	 @endif
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <a href="{{ url('bahan/add')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data </a>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          		<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th> No </th>
						<th> Id Kertas </th>
						<th> Jenis Kertas </th>
						<th> Aksi </th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1;?>
					@foreach($bahan as $row)
					<tr>
						<td> {{ $no++ }}</td>
						<td>{{ $row->id_bahan }}</td>
						<td>{{ $row->kertas }}</td>
						<td>
						<form method="post" action="{{ url('bahan/' . $row->id_bahan . '/delete')}}">
						<span class="pull">
						<a href="{{ url('bahan/' . $row->id_bahan . '/edit')}}" class="btn btn-primary"><i class="fa  fa-pencil"></i></a>
						
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<button class="btn btn-danger"><i class="fa  fa-trash"></i></button>
					</span>
				</form>
						</td>
					@endforeach
				</tbody>
			</table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
         
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection