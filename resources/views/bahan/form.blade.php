@extends('template.header')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
	  {{ empty($edit) ? 'Tambah' : 'Edit '}} bahan
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('bahan') }}">bahan</a></li>
        <li class="active">Data bahan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	@if(session('error'))
	  <div class="alert alert-danger">
		<p>{{ session('error') }}</p>
	  </div>
	 @endif
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
			<a href="{{ url('bahan') }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i>Kembali</a>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="container">
			<div class="row">
				<form class="form-horizontal" method="POST" action="{{ empty($edit) ? url('bahan/add') : url('bahan/' . $result->id_bahan . '/edit')}}">
					{{ csrf_field() }}
					
					@if(!empty($edit))
					{{ method_field('PATCH') }}
					@endif
		<div class="form-group">
			<label class="col-sm-2 control-label">Bahan</label>
			<div class="col-sm-4">
				<input type="text" name="kertas" class="form-control" placeholder="kertas" value="{{ @$result->kertas }}"/>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label"></label>
			<div class="col-sm-4">
				<button type="submit" class="btn btn-primary" value="Submit" name="submit">Submit</button>
			</div>
		</div>
		
	</form>
			</div>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection	