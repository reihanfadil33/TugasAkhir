@extends('template.header')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
	  {{ empty($edit) ? 'Tambah' : 'Edit '}} Pembayaran
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('pembayaran') }}">Pembayaran</a></li>
        <li class="active">Edit Pembayaran</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	@if(session('error'))
	  <div class="alert alert-danger">
		<p>{{ session('error') }}</p>
	  </div>
	 @endif
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
			<a href="{{ url('pembayaran') }}" class="btn btn-info"><i class="fa fa-plus"></i>Kembali</a>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="container">
			<div class="row">
				<form class="form-horizontal" method="POST" action="{{ empty($edit) ? url('pembayaran/add') : url('pembayaran/' . $result->id_pembayaran . '/edit')}}">
					{{ csrf_field() }}
					
					@if(!empty($edit))
					{{ method_field('PATCH') }}
					@endif
		<div class="form-group">
			<label class="col-sm-2 control-label">Id Pembayaran</label>
			<div class="col-sm-4">
				<input type="text" name="id_pembayaran" class="form-control" readonly="true" value="{{ @$result->id_pembayaran }}"/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Id User</label>
			<div class="col-sm-4">
				<input type="text" name="id_user" class="form-control" readonly="true" value="{{ @$result->id_user }}"/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Id Stiker</label>
			<div class="col-sm-4">
				<input type="text" name="id_stiker" class="form-control" readonly="true" value="{{ @$result->id_stiker }}"/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Pieces</label>
			<div class="col-sm-4">
				<input type="text" name="pcs" class="form-control" readonly="true" value="{{ @$result->pcs }}"/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Total Harga</label>
			<div class="col-sm-4">
				<input type="text" name="totalharga" class="form-control" readonly="true" value="{{ @$result->totalharga }}"/>
			</div>
		</div>		
		
		<div class="form-group">
			<label class="col-sm-2 control-label">Status</label>
			<div class="col-sm-4">
				<input type="text" name="status" class="form-control" placeholder="Status" value="{{ @$result->status }}"/>
			</div>
		</div>	
		<div class="form-group">
			<label class="col-sm-2 control-label"></label>
			<div class="col-sm-4">
				<button type="submit" class="btn btn-primary" value="Submit" name="submit">Submit</button>
			</div>
		</div>
		
	</form>
			</div>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection	