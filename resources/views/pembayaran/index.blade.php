@extends ('template.header')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Info Pembayaran
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pembayaran</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	@if(session('success'))
	  <div class="alert alert-success">
		<p>{{ session('success') }}</p>
	  </div>
	 @endif
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
         <h4> Info Pembayaran </h4>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          		<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th> No </th>
						<th> Id Pembayaran </th>
						<th> Id User </th>
						<th> Id Stiker </th>
						<th> Pcs </th>
						<th> TotalHarga </th>
						<th> Status </th>
						<th> Action </th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1;?>
					@foreach($pembayaran as $row)
					<tr>
						<td> {{ $no++ }}</td>
						<td>{{ $row->id_pembayaran }}</td>
						<td>{{ $row->id_user }}</td>
						<td>{{ $row->id_stiker }}</td>
						<td>{{ $row->pcs }}</td>
						<td>{{ $row->totalharga }}</td>
						<td>{{ $row->status }}</td>
						<td><a href="{{ url('pembayaran/' . $row->id_pembayaran . '/edit')}}" class="btn btn-primary"><i class="fa  fa-pencil"></i> Edit </a>
							<form method="post" action="{{ url('pembayaran/' . $row->id_pembayaran . '/delete')}}">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}
							<button class="btn btn-danger"><i class="fa  fa-trash"></i> Delete </button>
							</form>
						</td>
					@endforeach
				</tbody>
			</table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
         
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection